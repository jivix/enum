<?php

declare(strict_types=1);

namespace Jivix\Enum\Tests\Unit;

use Jivix\Enum\Enum;
use PHPUnit\Framework\TestCase;
use UnexpectedValueException;

class EnumTest extends TestCase
{
    public function testCreation()
    {
        $foo = new class('foo') extends Enum
        {
            const FOO = 'foo';
            const BAR = 'bar';
        };

        $this->assertEquals('foo', $foo->getValue());
    }

    public function testExceptionThrown()
    {
        $this->expectException(UnexpectedValueException::class);

        $invalid = new class('invalid') extends Enum
        {
            const FOO = 'foo';
            const BAR = 'bar';
        };
    }
}