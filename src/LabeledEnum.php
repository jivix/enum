<?php

declare(strict_types=1);

namespace Jivix\Enum;

abstract class LabeledEnum extends Enum
{
    /**
     * An array of labels.
     *
     * @var array
     */
    protected static array $labels;

    /**
     * Returns the label.
     *
     * @return string
     */
    final public function getLabel(): ?string
    {
        return static::$labels[$this->value] ?? null;
    }

    /**
     * Returns instance from label.
     *
     * @param string $label
     * @return LabeledEnum
     */
    final static public function fromLabel($label): LabeledEnum
    {
        $value = array_search($label, static::$labels, true);
        return new static($value);
    }

    /**
     * Returns value as string.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getLabel();
    }
}