<?php

declare(strict_types=1);

namespace Jivix\Enum;

use BadMethodCallException;
use ReflectionClass;
use ReflectionException;
use RuntimeException;
use UnexpectedValueException;

abstract class Enum
{
    /**
     * The constant value.
     *
     * @var null|bool|int|float|string
     */
    protected $value;

    /**
     * An array of available constants.
     *
     * @var array
     */
    protected static array $constants = [];

    /**
     * Enum constructor.
     *
     * @param null|bool|int|float|string $value
     */
    final public function __construct($value)
    {
        if (!$this->isValid($value)) {
            throw new UnexpectedValueException('Invalid value for Enum "' . get_called_class() . '"');
        }

        $this->value = $value;
    }

    /**
     * Get an instance of the Enum subclass using given value.
     *
     * @param mixed $value
     * @return Enum
     */
    final public static function fromValue($value): Enum
    {
        return new static($value);
    }

    /**
     * @return mixed
     */
    final public function getValue()
    {
        return $this->value;
    }

    /**
     * Returns the enum name (i.e. the name of the subclass constant).
     *
     * @return string
     */
    final public function getName(): string
    {
        return static::search($this->value);
    }

    /**
     * Compares two subclass instances of Enum.
     *
     * @param Enum $enum
     * @return bool
     */
    final public function equals(Enum $enum)
    {
        return $this->getValue() === $enum->getValue() && get_called_class() == get_class($enum);
    }

    /**
     * Returns array with all names of the Enum subclass.
     *
     * @return string[]
     */
    final public static function names()
    {
        return array_keys(static::toArray());
    }

    /**
     * Returns array with all instances of the Enum class.
     *
     * @return static[]
     */
    final public static function values()
    {
        $values = [];
        foreach (static::toArray() as $key => $value) {
            $values[$key] = new static($value);
        }
        return $values;
    }

    /**
     * Returns all subclass constants as array [name => value].
     *
     * @return mixed[]
     */
    final public static function toArray()
    {
        try {
            $class = get_called_class();
            if (!array_key_exists($class, static::$constants)) {
                $reflection = new ReflectionClass($class);
                static::$constants[$class] = $reflection->getConstants();
            }
            return static::$constants[$class];
        } catch (ReflectionException $e) {
            throw new RuntimeException($e->getMessage());
        }
    }

    /**
     * Return constant name for value.
     *
     * @param $value
     * @return string
     */
    public static function search($value): string
    {
        return array_search($value, static::toArray(), true);
    }

    /**
     * Check valid value.
     *
     * @param $value
     * @return bool
     */
    final public static function isValid($value): bool
    {
        return in_array($value, static::toArray(), true);
    }

    /**
     * Check valid name.
     *
     * @param string $name
     * @return bool
     */
    final public static function isValidName(string $name): bool
    {
        $array = static::toArray();
        return isset($array[$name]);
    }

    /**
     * Returns instance when called statically (e.g. Foo::BAR() where BAR is a constant defined in Foo subclass).
     *
     * @param string $name
     * @param array $arguments
     * @return Enum
     * @throws BadMethodCallException
     */
    final public static function __callStatic(string $name, array $arguments): Enum
    {
        $array = static::toArray();

        if (isset($array[$name])) {
            return new static($array[$name]);
        }
        throw new BadMethodCallException('No static method or constant "' . $name . '" in class ' . get_called_class());
    }

    /**
     * Returns value as string.
     *
     * @return string
     */
    public function __toString()
    {
        return (string)$this->value;
    }

    /**
     * Returns instance from native value.
     *
     * @param $value
     * @return Enum
     */
    public static function of($value): Enum
    {
        return static::fromValue($value);
    }

    /**
     * Returns instance from native value.
     *
     * @param $value
     * @return Enum
     */
    public static function fromNative($value): Enum
    {
        return static::fromValue($value);
    }

    /**
     * Returns the native value.
     *
     * @return mixed
     */
    public function getNativeValue()
    {
        return $this->getValue();
    }
}